import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'DemoKeyDown';

  
//handle keyboard event
valueK: string='';
onKeyDownEnter(event:any) {
  this.valueK = 'vous avez pressé la touche entrée' ;
}

//handle keyboard event with specific key
valueK2: string='';
onKeyDown(event:KeyboardEvent) {
  this.valueK2 = 'vous avez tapé ' + (<HTMLInputElement>event.target).value;
}

}
